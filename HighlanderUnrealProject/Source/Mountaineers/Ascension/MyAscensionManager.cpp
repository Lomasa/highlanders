// Fill out your copyright notice in the Description page of Project Settings.


#include "MyAscensionManager.h"

// Sets default values
AMyAscensionManager::AMyAscensionManager()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AMyAscensionManager::BeginPlay()
{
	Super::BeginPlay();

}

void AMyAscensionManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AMyAscensionManager::OnRemoveArtifactOnWorld(TEnumAsByte<MyArtifactTypes> TargetArtifactType)
{
	ArtifactsOnWorld.RemoveSingle(TargetArtifactType);
}

void AMyAscensionManager::OnAddArtifactOnWorld(TEnumAsByte<MyArtifactTypes> TargetArtifactType)
{
	ArtifactsOnWorld.Add(TargetArtifactType);
}

FMyArtifactGenerationData AMyAscensionManager::onGetArtifactGenerationData()
{	
	TArray<TEnumAsByte<MyArtifactTypes>> tempValidArtifactTypes = getValidArtifactsForSpawning();
	
	TPair<int, int> MinMax = GenerateMinMaxForArtifacts();

	UE_LOG(LogTemp, Warning, TEXT("AMyAscensionManager::FMyArtifactGenerationData - Min: %d Max: %d"), MinMax.Key, MinMax.Value);

	return generateArtifactGenerationData(tempValidArtifactTypes, MinMax.Key, MinMax.Value);
}

FMyArtifactGenerationData AMyAscensionManager::generateArtifactGenerationData(TArray<TEnumAsByte<MyArtifactTypes>> tempValidArtifactTypes, const int Min, const int Max)
{
	TEnumAsByte<MyArtifactTypes> randomArtifactValidType;
	FMyArtifactGenerationData targetArtifactGenerationData;
	targetArtifactGenerationData.ModifierArtifacts = 0;
	int numberOfArtifacts = FMath::RandRange(Min, Max);

	UE_LOG(LogTemp, Warning, TEXT("AMyAscensionManager::generateArtifactGenerationData - ArtifactCount: %d"), numberOfArtifacts);

	if (isWorldGenerationException()) {
		numberOfArtifacts--;

		int worldGenerationIndex = tempValidArtifactTypes.Find(MyArtifactTypes::AT_WorldGeneration);

		targetArtifactGenerationData.WorldGenerationArtifacts.Add(OnFetchValidGenerationArtifactFromAscensionData());
		tempValidArtifactTypes.RemoveAt(worldGenerationIndex);

		UE_LOG(LogTemp, Warning, TEXT("AMyAscensionManager::IsWorldGenerationException - NewArtifactCount: %d"), numberOfArtifacts);
	}

	for (int i = 0; numberOfArtifacts > i; i++) {

		int RandomIndex = FMath::RandRange(0, tempValidArtifactTypes.Num() - 1);

		if (tempValidArtifactTypes.Num() > 0) {
			randomArtifactValidType = tempValidArtifactTypes[RandomIndex];
		} else
			randomArtifactValidType = MyArtifactTypes::AT_None;

		switch (randomArtifactValidType)
		{
		case MyArtifactTypes::AT_Building:
			targetArtifactGenerationData.BuildingArtifacts.Add(OnFetchValidBuildingArtifactFromAscensionData());
			tempValidArtifactTypes.RemoveAt(RandomIndex);
			break;

		case MyArtifactTypes::AT_Modifier:
			targetArtifactGenerationData.ModifierArtifacts = OnFetchValidModifierArtifactFromAscensionData();
			tempValidArtifactTypes.RemoveAt(RandomIndex);
			break;

		case MyArtifactTypes::AT_WorldGeneration:
			targetArtifactGenerationData.WorldGenerationArtifacts.Add(OnFetchValidGenerationArtifactFromAscensionData());
			tempValidArtifactTypes.RemoveAt(RandomIndex);
			break;

		default:
			break;
		}

		UE_LOG(LogTemp, Warning, TEXT("AMyAscensionManager::generateArtifactGenerationData - tempValidArtifactTypes.Num(): %d"), tempValidArtifactTypes.Num());

	}

	return targetArtifactGenerationData;
}

TPair<int, int> AMyAscensionManager::GenerateMinMaxForArtifacts()
{
	TPair<int, int> targetMinMax;
	float allSpawningArtifacts = 0;
	float allGenerationArtifact = 0;

	if (ascensionData.SpawningArtifactData.BuildingArtifacts.Num() <= 0 &&
		ascensionData.SpawningArtifactData.ModifierArtifacts <= 0 &&
		ascensionData.SpawningArtifactData.WorldGenerationArtifacts.Num() <= 0
		) {
	} else {
		allSpawningArtifacts = ascensionData.SpawningArtifactData.BuildingArtifacts.Num() + ascensionData.SpawningArtifactData.ModifierArtifacts + ascensionData.SpawningArtifactData.WorldGenerationArtifacts.Num();
		allGenerationArtifact = getWorldGenerationArtifactsCountInWorld() + ascensionData.SpawningArtifactData.WorldGenerationArtifacts.Num();
	}

	UE_LOG(LogTemp, Warning, TEXT("AMyAscensionManager::GenerateMinMaxForArtifacts - allSpawningArtifacts: %f allGenerationArtifact: %f"), allSpawningArtifacts, allGenerationArtifact);
	
	targetMinMax.Key = FMath::FloorToInt(allSpawningArtifacts / allGenerationArtifact);
	targetMinMax.Value = FMath::CeilToInt(allSpawningArtifacts / allGenerationArtifact);

	return targetMinMax;
}

bool AMyAscensionManager::isWorldGenerationException()
{
	int WorldGenerationOnWorldCount = 1; //includes the worldGenerationArtifact that is removed when expanding the world

	for (size_t i = 0; i < ArtifactsOnWorld.Num(); i++)
	{
		if (ArtifactsOnWorld[i] == MyArtifactTypes::AT_WorldGeneration)
			WorldGenerationOnWorldCount++;
	}

	if (ascensionData.SpawningArtifactData.WorldGenerationArtifacts.Num() > 0 &&
		WorldGenerationOnWorldCount == 1) {
		return true;
	}

	return false;
}

int AMyAscensionManager::getWorldGenerationArtifactsCountInWorld()
{
	int WorldGenerationArtifactOnWorld = 0;

	for (int i = (ArtifactsOnWorld.Num() - 1); i >= 0; i--) {

		if (ArtifactsOnWorld[i] == MyArtifactTypes::AT_WorldGeneration)
			WorldGenerationArtifactOnWorld++;
	}

	return WorldGenerationArtifactOnWorld;
}

TArray<TEnumAsByte<MyArtifactTypes>> AMyAscensionManager::getValidArtifactsForSpawning()
{
	TArray<TEnumAsByte<MyArtifactTypes>> tempValidArtifactTypes;

	for (size_t i = 0; i < ascensionData.SpawningArtifactData.BuildingArtifacts.Num(); i++){
		tempValidArtifactTypes.Add(MyArtifactTypes::AT_Building);
	}

	for (size_t i = 0; i < ascensionData.SpawningArtifactData.ModifierArtifacts; i++){
		tempValidArtifactTypes.Add(MyArtifactTypes::AT_Modifier);
	}

	for (size_t i = 0; i < ascensionData.SpawningArtifactData.WorldGenerationArtifacts.Num(); i++){
		tempValidArtifactTypes.Add(MyArtifactTypes::AT_WorldGeneration);
	}

	if (0 >= tempValidArtifactTypes.Num())
		tempValidArtifactTypes.Empty();

	return tempValidArtifactTypes;
}

TEnumAsByte<MyArtifactGenerationType> AMyAscensionManager::OnFetchValidGenerationArtifactFromAscensionData()
{
	if (ascensionData.SpawningArtifactData.WorldGenerationArtifacts.Num() <= 0) {
		return TEnumAsByte<MyArtifactGenerationType>(MyArtifactGenerationType::AGT_None);
	}

	int tempIndex = FMath::RandRange(0, ascensionData.SpawningArtifactData.WorldGenerationArtifacts.Num() - 1);

	TEnumAsByte<MyArtifactGenerationType> tempGenerationType = ascensionData.SpawningArtifactData.WorldGenerationArtifacts[tempIndex];
	ascensionData.SpawningArtifactData.WorldGenerationArtifacts.RemoveAt(tempIndex);

	return tempGenerationType;
}

int AMyAscensionManager::OnFetchValidModifierArtifactFromAscensionData()
{
	if (ascensionData.SpawningArtifactData.ModifierArtifacts <= 0)
		return 0;

	ascensionData.SpawningArtifactData.ModifierArtifacts--;

	return 1;
}

TEnumAsByte<MyBuildingType> AMyAscensionManager::OnFetchValidBuildingArtifactFromAscensionData()
{
	if (ascensionData.SpawningArtifactData.BuildingArtifacts.Num() <= 0)
		return TEnumAsByte<MyBuildingType>(MyBuildingType::None);

	int tempIndex = FMath::RandRange(0, ascensionData.SpawningArtifactData.BuildingArtifacts.Num() - 1);

	TEnumAsByte<MyBuildingType> tempGenerationType = ascensionData.SpawningArtifactData.BuildingArtifacts[tempIndex];
	ascensionData.SpawningArtifactData.BuildingArtifacts.RemoveAt(tempIndex);

	return tempGenerationType;
}

